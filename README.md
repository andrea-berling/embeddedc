# EmbeddedC

Appunti del corso di Embedded C di Maggio 2020 tenuto da Giovambattista Murana per Modis Italia presi da Andrea
Berlingieri (andrea.berlingieri at modis dot com).

Le note sono prese originalmente in plaintext, convertite in Markdown, importate nell'applicazione Joplin
(https://joplinapp.org/), dove sono aggiunti tag e immagini.

## Formati degli appunti

Gli appunti vengono esportati in 3 formati:
  - PDF, per portabilità e facilità di consultazione
    - si trovano sotto la directory pdf
    - un PDF per ogni incontro
  - Markdown, per semplicità di modifica
    - si hanno una directory per il Notebook EmbeddedSystems, un file md per ogni incontro ed una
      cartella per le risorse (e.g. le immagini)
  - JEX, per essere importate in Joplin mantenendo anche i metadati (e.g. i tag)
    - un unico file .jex per l'intero Notebook EmbeddedSystems

## Contributi

Si apprezzano PR per correzioni o miglioramenti agli apppunti. Per fare modifiche è possibile fare
modifiche direttamente ai file in Markdown oppure è possibile importare le note in Joplin,
modificarle da lì ed esportarle.

Nel caso vogliate presentare una PR per delle note modificate con Joplin ricordate di esportare:
  - in Markdown
  - in PDF
    - per i nomi dei PDF togliete gli spazi (e.g. "Primo Incontro.pdf" diventa "PrimoIncontro.pdf")
  - in JEX

Cercate anche di rispettare la gerarchia delle directory descritta in [Formati degli appunti](#formati-degli-appunti).

<!-- vim:et:ts=2:sts=2:sw=2:
-->
