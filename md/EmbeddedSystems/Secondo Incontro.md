Secondo Incontro

In programmazione embedded si ha a che fare sostanzialmente con l'hardware

- l'hardware va a controllare il mondo esterno
    - in sistemi ad alto livello invece l'I/O è, solitamente, un testo
- serve una gestione oculata delle risorse
    - per necessità di risparmio sui costi o per requirements di prestazioni
- è necessario sapere come è realizzato il controllo con il microcontrollore e come è meglio  
    interfacciarsi con il mondo esterno

Finite State Machine

- sono un modo di rappresentare una realtà che evolve nel tempo
    
    - e.g. oggetti fisici complessi
    - assumono di volta in volta degli stati
        - i.e. situazioni descritte dal loro spazio degli stati
            - lo spazio degli stati è finito
- per essere modellato da una FSM un sistema deve poter essere descritto da:
    
    - variabili di ingresso controllabili
    - variabili di uscita osservabili
    - variabili di stato che non possono essere osservate o misurate direttamente
        - sono deducibili in base agli ingressi e alle uscite
        - rappresentato lo "stato del sistema"
            - determinano lo stato di uscita
- abbiamo due famiglie di FSM:
    
    - asincrone
        - il cambiamento può avvenire in ogni momento in presenza di un evento su un ingresso o sullo  
            stato stesso
    - sincrone
        - il cambiamento può avvenire solo in presenza di eventi discreti generati da un segnale di clock
            - i cambiamenti di stato, i valori di ingresso ed i valori di uscita risultano interessanti solo  
                negli instanti di sincronizzazione $t_{1},t_{2},\ldots,t_{k}$
- l'uscita dipende dallo stato
    
    - i.e. agli stessi ingressi possono corrispondere più uscite in momenti diversi
    - questo non è il caso per le porte combinatorie
- lo stato contiene le informazioni necessarie per calcolare l'uscita del sistema
    
    - tuttavia non è in generale determinabile la sequenza di ingressi che ha portato il sistema in  
        un certo stato
    - rappresenta, in maniera sintetica, la storia del sistema
- formata da
    
    - un insieme di simboli di ingresso $X$
    - un insieme di simboli di uscita $Z$
    - un insieme non vuoto di stati $\Sigma$
    - una funzione lambda che calcola l'uscita
        - nei sistemi sincroni mostrano la $k$-esima uscita in funzione del $k$-esimo ingresso e del  
            $k$-esimo stato
    - una funzione delta che calcola lo stato futuro
        - nei sistemi sincroni mostrano il $k$-esimo stato in funzione del $k$-esimo ingresso e del  
            $k$-esimo stato
    - questi insiemi si suppongono avere una cardinalità finita (realizzabilità)
- modello di Huffman di una FSM
    
    ![Huffman.png](../_resources/e5b79c3c395c45a394beb993ccacf5c4.png)
    
    - abbiamo un elemento di memoria che mantiene lo stato corrente della macchina
    - sono ben divise le due parti:
        - di logica combinatoria
            - il blocco lambda,delta
        - di memoria
    - si potrebbe pensare che si avrebbe un loop infinito di cambio di stato e calcolo di uscita  
        della rete lambda,delta
        - la porta combinatoria calcola l'uscita $z_{k}$ e lo stato $s_{k+1}$ a partire dall'entrata $x_{k}$ e lo  
            stato $s_{k}$
            - ma poi la porta combinatoria calcolerebbe immediatamente la nuova uscita $z_{k+1}$ e il nuovo  
                stato $s_{k+2}$ a partire dall'entrata $x_{k}$ e dallo stato stato $s_{k+1}$
                - ma poi la porta combinatoria calcolerebbe immediatamente la nuova uscita $z_{k+2}$
                    - ...e cosi' via
        - in realtà il processo avviene in istanti discreti
            - questa "discretizzazione" del tempo viene fatta dalla memoria
                - la memoria necessità dell'impulso di clock per cambiare valore
                - nel modello di Huffman è sottointeso che la memoria sia regolata da clock
                    - i.e. che non sia una memoria passante
    - si tratta di un modello semplificato
        - non coglie appieno il concetto di macchina a stati
        - può essere ulteriormente specializzato da altri modelli
            - modello di Mealy
                
                ![Mealy.png](../_resources/7cc94d5aa34a4a34a9807dd292fe7fed.png)
                
                - l'uscita dipende dallo stato corrente e dall'ingresso corrente
                - lo stato dipende dallo stato corrente e dall'ingresso corrente
            - modello di Moore
                
                ![Moore.png](../_resources/af9593b1f7ab45e686773e8c8af6b533.png)
                
                - l'uscita dipende esclusivamente dallo stato corrente
                - lo stato dipende dallo stato corrente e dall'ingresso corrente
            - in entrambi sono divise le funzioni lambda e delta in reti diverse
                
            - si può dimostrare che i due modelli sono equivalenti
                
                - la scelta tra i due modelli è per motivi implementativi
                    - "per comodità"
                - la capacità espressiva dei due modelli è la stessa
                - due macchine a stati finiti si dicono equivalenti se a parità di stato iniziale e per  
                    ogni sequenza di ingressi le uscite sono le stesse
                - per una sequenza di $j$ ingressi la macchina di Mealy genera $j$ uscite e la macchina di  
                    Moore genera $j+1$ uscite
                    - la macchina di Moore genera un'uscita legata allo stato iniziale $\sigma_{0}$ che possiamo ignorare se vogliamo confrontare i due modelli
- possono essere rappresentate mediante un grafo di transizione
    
    - grafo orientato
    - ad ogni stato corrisponde un nodo
    - ad ogni transizione di stato corrisponde un arco
        - per ogni coppia di stati appartennte alla relazione (stato presente,stato futuro) abbiamo un  
            arco che va dal primo al secondo
    - in base al modello scelto cambiano le etichette dei nodi e degli archi
        - macchina di Mealy
            
            ![MealyGraph.png](../_resources/4ea51ea9026444fe9c5a1339b1fb3659.png)
            
            - stati nei nodi, simboli di entrata a di uscita sugli archi
        - macchina di Moore
            
            ![MooreGraph.png](../_resources/4564998b92f44b51a25322ffc68a2f03.png)
            
            - stati nei nodi e uscite nei nodi, ingressi sugli archi
                - questo perche' l'uscita dipende solo dallo stato
    - un grafo di transizione non è un diagramma di flusso
        - non dice niente sul "come" riconoscere un simbolo (e.g. un carattere)
        - è più "dichiarativo" di un diagramma di flusso (che e' invece più "procedurale")
- possono essere rappresentate mediante una matrice di transizione
    
    - una riga per ogni stato, una colonna per ogni simbolo di ingresso
        
        - una rappresentazione duale (una riga per simbolo, una colonna per stato) sarebbe equivalente
    - la casella $(\sigma_{i},\xi_{k})$ contiene i valori di stato futuro e uscita corrente nella macchina  
        di Mealy
        
        ![MealyMatrix.png](../_resources/eda37ab7532c426db67c07b57eeb8646.png)
        
    - la casella $(\sigma_{i},\xi_{k})$ contiene lo stato futuro nella macchina di Moore
        
        ![MooreMatrix.png](../_resources/f47d8050e3894f9ebc9160985e03c3d1.png)
        
        - le uscite sono rappresentate in un'ulteriore colonna nella tabella che non corrisponde a  
            nessun ingresso
        - questa è molto più semplice da utilizzare al momento della programmazione
    - in generale dovremmo rappresentare tutto l'alfabeto nella matrice di transizione
        
        ![WholeTable.png](../_resources/dc5796d6204e4972894be03165817ed5.png)
        
        - in pratica per compattezza possiamo restringersi ai soli simboli che portano a stati  
            significativi e usare un default per gli altri simboli
            
            ![ReducedTable.png](../_resources/4b433f3193da41dbbf97601ca00f0d96.png)
            
            - a livello di programmazione questo risulta in minor codice
- in una FSM esistono due stati speciali:
    
    - stato sorgente
        - stato iniziale
        - definizione: uno stato da cui esistono solo archi uscenti
    - stato pozzo
        - stato finale
        - rappresentato con un doppio cerchio nel diagramma di un grafo di transizione
        - definizione: uno stato da cui esistono solo archi entranti

Le vecchie telescriventi mandavano una codifica del carattere battuto al sistema trasmittente e  
ricevevano una codifica dal sistema ricevente

- in entrambi i casi servivano un numero di fili basato sul numero di bit della codifica
    - di conseguenza poteva risultare conveniente utilizzare 5 bit per carattere
- in ASCII ci sono alcuni caratteri speciali legati all'hardware del tempo
    - e.g. backspace, bell, line feed, horizontal tab, carriage return, vertical tab, STX (start of  
        transmission), ETX (end of transmission), ENQ (notizie sullo stato), ACK, NAK, EOT (end  
        of text)

I bit utilizzati per rappresentare i numeri, le lettere e anche il codice eseguibile sono gli stessi

- come si distingue tra questi? In base al contesto
    - e.g. a volte il numero 0x03 puo' essere interpretato come il numero positivo 3, a volte come  
        il carattere speciale ASCII di ETX, e a volte come un'istruzione in linguaggio macchina, ad  
        esempio di ADD
    - a volte può essere utile cambiare il contesto di interpretazione
        - e.g. mediante casting in C

Architettura di Von Neumann

![VonNeumann.png](../_resources/b9b86e3eb8fc4f06b9b735d505085079.png)

- blocchi funzionali collegati da un bus
    - in sostanza una "mazzata" di fili che portano una serie di segnali con un certo compito
        - e.g. bus dati, bus controllo, etc.
- blocchi principali: CPU, Memoria, Dispositivi di I/O
    - nella CPU abbiamo un ulteriore bus "privato" che collega la ALU, i registri, l'unità di  
        controllo, etc.
        
        ![CPUBus.png](../_resources/9e4610b757d7489a9320c6d0dcbac047.png)
        
        - una ALU è sostanzialmente una rete combinatoria
            - a differenza di una FPU, che è più una macchina a stati
        - l'unità di controllo è una macchina a stati finiti che gestisce il microcodice
            - una CPU viene programmata mediante istruzioni in linguaggio macchina poste all'interno  
                della memoria
                - questo va sotto il controllo del programmatore
            - il codice che va decodificare un'istruzione fa parte del cosiddetto "microcodice"
                - è ciò che traduce, ad esempio, un codice di 0x03 in un'operazione di somma tra due  
                    registri
                - sta all'interno della CPU
                - non è sotto il controllo del programmatore
        - rispetto alla macchina di Turing
            - alla testina corrisponde la CPU
            - alla memoria il nastro infinito corrisponde
            - possiamo pensare alla macchina di Von Neumann come una macchina di Turing che utilizza  
                le componenti sopracitate
    - la prima cosa che bisogna fare quando viene avviata una macchina di Von Neumann è portare  
        tutti gli oggetti all'interno della CPU ad uno stato definito
        
        - questo viene fatto mediante un segnale di reset
            - per motivi pratici non viene resettato tutto
                - e.g. la memoria non viene resettata
                - e.g. il program counter viene resettato
                    - è importante sapere, nei sistemi embedded, da dove si parte ad eseguire il codice
                        - questo dipende dall'architettura della CPU
                            - si potrebbe partire, a priori, da un qualsiasi indirizzo
                                - di solito si parte da indirizzi che sono multipli di 16
                                - la prima parte di indirizzi è solitamente riservata per scopi speciali (e.g.  
                                    interrupt vector)
                                - un valore tipico di partenza per il program counter potrebbe essere 0x100
                                    - quando si flasha la ROM un chip bisogna porre attenzione a mettere il codice  
                                        nel posto giusto
                            - questa informazione è contenuta nel datasheet del processore

Tra i vari registri presenti in una CPU abbiamo:

- Instruction Register
    - è unidirezionale
        - e.g. il PC, invece, è bidirezionale
            - si può sia leggere che scrivere
    - in esso viene posto il codice di un'istruzione prelevato dalla memoria
        - viene poi usato dal microcodice per decodificare l'istruzione ed eseguirla
- Memory Address Register e Memory Data register
    - si occupano della gestione della memoria

La ALU è un circuito combinatorio con due ingressi, dei fili di controllo ed un'uscita

- chi decide quali segnali attivare?
    - la Instruction Decoder and Control Logic
- di solito un operatore viene da un registro temporaneo e l'altro dalla memoria
    - l'uscita può essere posta in uno dei due operandi, in un altro registro, o anche direttamente  
        in memoria

In una macchina un dato può essere considerato, a seconda del contesto, un dato vero e proprio  
oppure un'istruzione

CPU Z80

![Z80.png](../_resources/2971b97b70ee4b5f8203faf019c46dd0.png)

- architettura mista
    - address bus e control bus sono usate per comunicare con il mondo esterno
    - all'interno esiste un data bus che interconnette le varie unità del processore
- i registri sono duplicati
    - tranne quelli speciali
    - e.g. a W corrisponde W', a Z corrisponde Z', etc.
    - questo rendeva semplice e veloce salvare i valori dei registri
        - e.g. per un context switch

ARM

![ARM.png](../_resources/fb9d364a111241f1bad21702f67b5911.png)

- architettura che va per la maggiore oggi
- i bus esterni sono sconnessi da quelli interni mediante dei registri di "isolamento"
    - questo consente al processore di eseguire le sue operazioni una volta che sono stati caricati  
        gli operandi
    - le operazioni del processore sono svincolate da quelle della memoria

Un processore nasce per un valore minimo ed uno massimo di velocità

- maggiore la velocità maggiore sarà il consumo energetico e la temperatura
    - gran parte dell'energia consumata viene persa nei transitori
        - mantenere un certo livello di tensione richiede poca energia
- va calibrato in base alle necessità e ai constraint legati al sistema
    - e.g. limiti di temperatura, di batteria, etc.
- la velocità del processore è legata ad un componente chiamato oscillatore
    - è ciò che crea il segnale ad onda quadra che governa tutte le componenti di una macchina
    - è sostanzialmente un cristallo di quarzo che, per effetto piezoelettrico, si mette ad  
        oscillare se opportunamente stimolato da un campo elettrico
        - questi risuonano ad una particolare frequenza di risonanza
            - dalle decine di kHz alle decine di MHz
            - la frequenza è legata alla dimensione del cristallo
                - più è piccolo il cristallo maggiore è la frequenza
            - un quarzo risuona bene per la frequenza per cui è stato tagliato
                - stimolare in modo diverso il cristallo per variare la velocità della CPU richiede delle  
                    modifiche al circuito di risonanza
                    - generalmente si rischia di ottenere dei risultati deludenti
                        - per ottenere un effetto migliore sono stati creati i PLL (Phase Locked Loop)

Phase Locked Loop

![PLL.png](../_resources/3e1936e3432643d6bc7217fb19f0d3f8.png)

- una volta i circuiti di clock erano esterni ai processori
    - oggi sono interni mediante PLL
- permettono di scegliere la velocità del processore
    - entro certi limiti anche a runtime
        - e.g. in base al tipo di alimentazione (dalla batteria, dalla presa della corrente, etc.)
- è controllato da una serie di registri
    - importante conoscerli e comprendere l'effetto di agire su questi registri
- è composto da:
    - un comparatore di fase
        
        ![Comparator.png](../_resources/b390a3c0da6141929508bfed905bf44b.png)
        
        - emette un segnale che è proporzionale alla differenza di fase tra i due segnali in ingresso
        - se i segnali non sono in fase darà un segnale proporzionale alla differenza di fase
            - questa differenza si andrà poi ad annullare nel PLL
                - l'oscillatore regolerà la sua frequenza fino ad ottenere una frequena comparabile con  
                    quella di riferimento
                    - la frequenza di uscita F_out risulterà stabile
        - è concettualmente equivalente ad uno XOR
            - nella pratica ci sono problemi legati alla realizzazione fisica
    - un filtro passa basso
        
        ![LowPassFilter.png](../_resources/8dee9d25fc574310b807a71287f2a6ef.png)
        
        - un filtro per far passare frequenze sotto un certo valore
        - è concettualmente equivalente ad un circutio chiamato "integratore"
            - una resistenza più un condensatore a terra
        - approssima un valore continuo a partire da un'onda quadra data dal comparatore di fase
    - un circuito controllato in tensione (VCO, Voltage Controlled Oscillator)
        
        ![VCO.png](../_resources/3ec636032f27497f982fa5b628464139.png)
        
        - un oscillatore la cui frequenza dipende dalla tensione di controllo
    - opzionalmente un circuito di divisione
        
        ![Divider.png](../_resources/e1095c3e10c04189bf401808afc17615.png)
        
        - utilizzato per produrre una frequenza superiore a quella di riferimento
        - nonostante si chiami "divisore" la frequenza in uscita è un multiplo di quella di controllo
            - se il divisore riduce la frequenza che verrà data in input al comparatore l'oscillatore  
                dovrà produrre una frequenza maggiore affinché la frequenza ridotta diventi comparabile  
                con la frequenza di riferimento
        - si realizza mediante un contatore
        - l'unica cosa su cui abbiamo controllo come programmatori
            - abbiamo un registro che determina il valore del divisore
                - i valori validi sono presenti nel datasheet del processore
- è sempre legato ad un oscillatore
    - potrebbe essere un cristallo di quarzo oppure un oscillatore ceramico
        - quelli ceramici sono meno precisi per quanto riguarda la frequenza base e la stabilità  
            rispetto alle variazioni di temperature in confronto a quelli al quarzo
    - si possono usare oscillatori RC
        - i valori di Resistenza e Capacità sono calibrati in fabbrica e danno un valore di  
            frequenza più o meno determinato
            - entro i margini di incertezza dati dal costruttore
- limita le emissioni a radiofrequenza (EMI)
- prende una frequenza di riferimento in ingresso
- "dividi per moltiplicare"
    - l'output del VCO viene diviso per renderlo comparabile con quello di riferimento
- lavorare con il PLL richiede cautela
    - bisogna serve basarsi sul datasheet del processore
- esiste un segnale di lock
    - viene attivato quando il PLL è agganciato in fase
        - va fatto un check prima di inizializzare le altre parti del processori per avere la garanzia  
            che il PLL sia agganciato al segnale di riferimento (e quindi stabile)

Nei processori della famiglia STM32 abbiamo anche un blocco di divisione

- possiamo quindi moltiplicare la frequenza di riferimento per un numero razionale

I circuiti di temporizzazione sono una delle prime rogne quando si va a realizzare una scheda

- da questi discende la stabilità e l'affidabilità di tutto il sistema

Una delle differenze principali dei processori odierni da quelli tradizionali è che i processori  
mirano ad essere i più veloci possibile

- questo viene realizzato usando anche delle memorie interne chiamate "cache"
    
    ![Cache.png](../_resources/f5cd2ab956bd446b869053fcc96d04c5.png)
    
    - si memorizzano parte di dati, istruzioni, operandi, etc. che stanno nella memoria principale  
        quando vengono prelevate dalla memoria principale
    - prendere i dati dalla memoria richiede del tempo
        - per risparmiare si utilizza una memoria veloce ed ottimizzata
        - i dati arrivano sia alla CPU che alla cache
        - ad ogni ciclo il cache controller fa un lookup basato su un indirizzo nella cache per vedere  
            se il dato o l'isruzione sono già stati salvati
            - se si trova in cache (cache-hit) viene caricato da lì più velocemente
            - se non si trova in cache (cache-miss) viene caricato dalla memoria, con una penalità di  
                tempo
            - se il rapporto di cache-hit/cache-query è abbastanza alto si riducono gli accessi alla  
                memoria e migliorano le prestazioni
                - si parla di miglioramenti di un fattore 1:50 o addirittura 1:100 in base alla politica  
                    di gestione della cache
- un altro incremento delle prestazioni si ottiene dal pipelining
    
    - i.e. esecuzione di più frammenti di istruzioni in parallelo
    - questo assume che la prossima istruzione che verrà eseguita sarà quella successiva in  
        memoria
        - in caso di jump occorre invalidare la pipeline
        - in base al tipo di codice eseguito la pipeline può abbassare le prestazioni del sistema
