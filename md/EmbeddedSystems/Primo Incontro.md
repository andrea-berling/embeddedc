Primo Incontro

Docente: Gianni Murana, Roma, progettazione Embedded (SW-HW), responsabile laboratorio

In Embedded ci sono differenze rispetto a sistemi più grandi

- l'hardware per vari motivi può essere limitato
    - il firmware ne deve tenere conto
        - servono particolari cautele e dettagli sulla progettazione

Stretta dipendenza dell'hardware dal firmware

In Embedded i sistemi di I/O hanno a che fare con sensori, attuatori, etc.

L'elemento in comune con macchine più grandi è il linguaggio di programmazione: solitamente il C

- non è limitante: non tutti i sistemi embedded devono essere programmati in C
    - si possono usare linguaggi assemblativi, sistemi di traduzione da altri ambienti (e.g. Simulink, Matlab, etc.)

Un sistema embedded si può occupare di controllo e attuazione

- e.g. braccio meccanico
- parte sensori, parte attuatori, parte controllo
    - noi ci occuperemo della parte di controllo

Un sistema embedded comprende molte applicazioni

- e.g. in un aereo sistemi di volo, sistemi del carburante, sistemi di condizionamento
    dell'abitacolo, etc.

Un sistema embedded non è un sistema di dimensioni ridotte

- possono essere sistemi embedded il sistema di controllo di un drone (piccolo) o quello di un
    motore marino (grande)

Macchine di calcolo (meccaniche):

- semplificare le operazioni
- aumentare l'affidabilità dei calcoli
- questi sono anche requisiti dei sistemi embedded
    - e.g. sostituzione nel controllo di processo di un sistema manuale con uno automatico (PLC)
        - PLC: interfacce più standardizzate, linguaggi di programmazione come PL/I o PL/M, etc.

Macchine di calcolo (elettroniche)

- perché passare all'elettronico? Per comodità

I sistemi embedded tradizionali, siano essi elettronici o meccanici, tendono a non essere
programmabili

Macchine a programma memorizzato

- pianoforte a rullo
    - ha caratteristiche di programmabilità
        - sostituendo il rullo si può cambiare la melodia (il "programma")
    - realizzazione completamente meccanica di un sistema embedded
- organetto a nastro perforato
    - programma rappresentato da un foglio perforato
- forti limitazioni rispetto ai sistemi embedded moderni: non si può cambiare la melodia in base
    ad un feedback esterno a runtime

Embedded:

- è sistema realizzato con uno scopo specifico
- è diverso da sistema per uso generale (e.g. un PC)
    - può essere utilizzato in un sistema di uso generale (e.g. sistema di controllo della
        temperatura di un PC)

Realizzazione sistema embedded

- componenti discreti
    - circuiti logici, microprocessori, circuiti analogici
- schede configurabili
    - microcontrollori, FPGA
- anche hard wired
    - e.g. con porte logiche
        - questi non sono intrinsicamente configurabili, ma lo diventano in base ad una configurazione esterna

National Instruments

- moduli programmabili con interfacce standard e funzioni specifiche
    ![NationalInstruments.png](../_resources/773972291bf2478eb713a7619c93add7.png)

- vantaggi:

    - semplicità di programmazione

    - si possono usare diversi linguaggi

        - C, LabView, Simulink, etc.
    - modularizzazione sistema

- svantaggi

    - costo
    - ingombro
    - un sistema ad hoc può essere utilizzato invece con costi minori e ingombro ridotto
        - occhio: esistono costi nascosti (non ripetitivi) che in un sistema basato su moduli non
            esistono
- sistemi a moduli:

    - in generale preferibile per applicazione specifica senza produzione di massa
        - questo criterio non è sempre valido, ma abbastanza indicativo
    - ci possono essere vincoli di altro tipo
        - e.g. di ingombro, di dissipazione, etc.

Ci sono diverse variabili da considerare nella calibrazione di un sistema embedded

- complessità, costo, numero di pezzi da produrre, etc.

Due scale di costi da considerare:

- costi non ricorrenti (minimi-massimi)
- costi ricorrenti (minimi-massimi)

Ci focalizzeremo sulla logica elettronica

- è raro trovarsi a lavorare con sistemi embedded meccanici

Due valori: 0 e 1 (Vero-Falso)

- Ambizione di Boole: creare un sistema che, a partire da delle proposizioni, arrivasse a delle
    conclusioni corrette senza che vengano fatti passi falsi intermedi
    - i.e. avere una dimostrazione rigorosa

Perché due livelli, piuttosto che 10 (base decimale)?

- ci serve un sistema semplice per discriminare vari livelli
    - e.g. sarebbe difficile usare 10 livelli di tensione con step da un volt
        - il circuito risulterebbe molto più complesso
            - ci serve un margine di indecisione (per via dei rumori nel circuito)
                - 1 volt non è mai 1 volt
                    - per vari motivi: caduta di tensione, alimentatore instabile, etc.
            - i circuiti rilevatori e trasmettitori diventano più complessi
                - e.g. servirebbero dei comparatori di tensione a 10 livelli

Esistono altre codifiche

- e.g. 2b1q, 4 livelli

Nel mondo reale non si utilizzano porte logiche con più di qualche decina di ingressi

- la velocità dei segnali non è la stessa su tutti gli ingressi
- se i tempi di elaborazione della porta logica sono brevi, il risultato rimane stabile
- se ci sono tanti ingressi, la probabilità di degradazione del segnale e di un cambio
    indesiderato di valore di output aumenta
- si possono mettere a catena varie porte per simulare porte a più ingressi
    - ma anche questo può creare problematiche di ritardi
        - le cosiddette "alee" dei circuiti
        - vanno fatte considerazioni fisiche

È importante:

- capire i meccanismi con cui sono realizzate le porte logiche e come funzionante
    - per capirne i limiti e per comprendere i motivi di certe scelte di design
- imparare a leggere schemi logici
    - queste porte possono essere trovate su board create da altri e può essere necessario
        manipolarle per implementare un certo controllo

Circuiti combinatori: il risultato dipende solo dagli ingressi (a meno dei tempi di propagazione)

- e.g. ci aspettiamo che l'uscita sia sempre 1 se abbiamo due 1 agli ingressi di una porta AND
    - in realtà avremo un transitorio iniziale per via dei tempi di propagazione in cui non avremo
        il valore di uscita corretto
        - "non voluto" (nei circuiti combinatori)
- si tratta nella di sistemi deterministici, a meno di tempi di propagazione e transitori

Sistemi sequenziali: tengono memoria di quello che è accaduto prima

- hanno comunque un comportamento affidabile
    - i.e. non sono sistemi aleatori

Flip-flop

- il nome deriva dalla presenza di due stati stabili, variabili in base ai segnali di ingresso e
    al tipo di variazione

Se abbiamo un segmento di circuito collegato, mediante una resistenza di pullup, alla tensione alta e lo colleghiamo alla massa mediante un cavo (resistenza 0), misureremo una tensione di 0 V in quel segmento

- un discorso duale si applica in caso di resistenze di pull-down ed un cavo a tensione
- nel caso collegassimo il segmento di circuito a massa mediante una resistenza creeremmo un
    divisore di tensione
    - nei circuiti digitali questo solitamente non e' desiderabile, per non andare a creare valori
        di tensione "ambigui"
        - i.e. non facilmente classificabili come un 1 o uno 0

Il latch SR è un primo esempio di memoria statica (non-volatile)

7400 indica che si tratta di un circuito TTL con 4 porte NAND

Quando viene acceso un latch SR lo stato di Q è undefined

- i.e. può essere 0 o 1, in base al tempo di propagazione dei segnali
- all'accensione non è determinabile lo stato iniziale delle memorie
- nei sistemi embedded non è mai predicibile il valore di una cella di memoria mai utilizzata
    - MAI fare l'assunzione che sia 0
    - bisogna fare in modo che le celle di memoria abbiano uno stato determinato

Non sempre si utilizza il valore corretto di default di una memoria

- e.g. il programmatore talvolta si scorda di inizializzare una cella di memoria, e ottiene dei risultati
    impredicibili
- bisogna SEMPRE inizializzare i valori delle memorie

La D in Flip-Flop di tipo D sta per "Delay"

- rallenta di un colpo di clock la propagazione di un segnale
    - in SR la propagazione è "immediata"
        - a meno dei tempi di propagazione del circuito
- le variazioni avvengono in corrispondenza delle salite e discese del segnale
- per dare uno stato affidabile ad un flip flop di tipo D vengono spesso resi disponibili dei
    segnali di Preset e Clear
    - Preset -> Q = 1 (regardless)
    - Clear -> Q = 0 (regardless)

Stadio di uscita

- totem pole

    - si realizza una uscita in cui il pin di uscita viene portato a ground o a tensione mediante un
        circuito con BJT NPN e PNP o due MOSFET in base al valore di uscita

        ![TotemPole.png](../_resources/40cf5b1ca1f040c78d79ea6af20ebee6.png)

        - il potenziale in uscita sarà necessariamente più basso
            - la tensione di polarizzazione è circa di 0.6-0.7 volt ed il transistor ha un drop di 0.2
                volt tra emettitore e collettore
            - importante da tenere a mente durante misurazioni con oscilloscopio o multimetro
            - questo è un altro motivo per cui utilizziamo due soli livelli di tensione
                - usare 10 livelli diventa infattibile con le incertezze coinvolte
    - a livello schematico:

        ![TotemPoleEquivalent.png](../_resources/ba5a34f684634f70bcb06526985134fc.png)

    - cosa succede se si collegano direttamente due uscite totem pole?

        - in certi casi si può avere un corto circuito

            ![TotemPoleCollision.png](../_resources/092a7b5874f54f849fb3cb83e7026a38.png)

        - conduzione simultanea

            - a volte è usata di proposito
                - solo se c'e' una resistenza di mezzo, o per esempio per l'accensione o spegnimento
                    di un motore
                - nei circuit logici pero' non e' mai utilizzata di proposito
            - va prestata attenzione
        - perché utilizzare totem pole invece che dare in uscita direttamente il segnale?

            - potremmo voler realizzare funzioni logiche hard-wired
            - potremmo non conoscere a priori il numero di dispositivi logici utilizzati
                - e.g. schede di memoria nei PC
        - si può utilizzare lo stadio di uscita three-state

- collettore aperto

    - oppure open drain, se si usa un MOSFET piuttosto che un BJT

    - come totem pole ma senza la parte sopra

        ![OpenCollectorDiagram.png](../_resources/b1895cb5fa8a47559e2eab81f34c5691.png)

        - si può solo chiudere il transistor verso massa
        - interruttore chiuso, out a ground
        - interruttore aperto, out floating
    - Diagrammi logici:

        ![OpenCollector.png](../_resources/df6181f3710d4931be728c3392a4e6cd.png)

    - non si possono avere collisioni

        - è instrinsicamente sicuro a livello hardware
            - non lo è a livello software
        - tuttavia non possiamo, di default, portare l'output a 1
            - per fare ciò ci basta utilizzare un pullup resistor
    - si possono realizzare

        - AND tra variabili High (Active-High logic)

        - OR tra variabili Low (Active-Low logic)

        - esempio di AND/OR:

            ![OpenCollectorGate.png](../_resources/0d170f78dcbd423e93451e0b3a4f9f65.png)

        - creare porte logiche modulari

- three-state

    - simbolo logico:

        ![ThreeStateLogicSymbol.png](../_resources/1ac68a1759b244a5953df7d9e07c3ac4.png)

    - realizzazione fisica:

        ![ThreeStateDetail.png](../_resources/3baa782374f4479f9677bdf8ad23f981.png)

    - a livello schematico:

        ![ThreeStateSchematic.png](../_resources/e7f548d1020d42b1862e3c3872df39cc.png)

    - i driver circuit possono essere in stato di alta impedenza

        - i.e. come se fosse disconnesso dal circuito

        - lo si ammette come terzo stato logico

        - viene utilizzato come il totem pole, mettendo la porta ad alta impedenza se inutilizzata

            - e.g. alloggiamenti per la memoria in un PC
    - i circuiti tri-state hanno di solito un pin in più di quelli strettamente necessari

        - il cosiddetto pin di enable (OE), con una Truth table del genere:

            ![ThreeStateTruthTable.png](../_resources/a2ed3b42c9cb48f09e48a3e46e400209.png)

    - le uscite tri-state permettono di collegare più uscite allo stesso nodo elettrico a formare
        un bus

        ![ThreeStateBus.png](../_resources/6a020728e9e94676a863a73c43868f81.png)

        - non bisogna mai abilitare più di un'uscita
            - si avrebbe una collisione come in totem pole
            - per sicurezza andrebbe usato un circuito di mutua esclusione
                - nella realtà per risparmiare denaro questo non viene fatto ed il controllo viene
                    realizzato a livello software nel firmware

Abbiamo due categorie principali di transistor:

- Bipolar Junction Transistor
    - consuma più corrente
    - semiconduttori bipolari
        - gli elementi sono prossimi alla conduzione, ma non nella banda di conduzione
            - vengono "drogati" con atomi di materiale specifico per portare gli elettroni prossimi alla
                banda di conduzione (ma ancora sotto)
            - mettendo a contatto un semiconduttore drogato P e uno drogato N si crea una banda in cui
                risulta semplice spostare elettroni dalla banda di valenza alla banda di conduzione
    - NPN
        - Negli schematics:

            ![NPN.png](../_resources/8a0e22027c1546eb8b4d8ef1766eff02.png)

        - parte alta: Collettore

            - "colleziona" energia dall'alimentazione
            - drogato N
        - parte centrale: Base

            - drogato P
        - parte centrale: Emettitore

            - emette energia solo se la base si trova ad un certo potenziale
                positivo rispetto all'emettitore, e quindi passa una certa corrente tra Base ed
                Emettitore
            - drogato N
        - PNP è analogo

            - la differenza deve essere tra base e collettore
                - i.e. la base va posta a low
        - consuma più energia

        - è più veloce

            - e.g. circuiti a radiofrequenza
- MOSFET
    - Metal Oxide Substrate Field Effect Transistor

    - Esempio di N-Channel type MOSFET:

        ![NMOS.png](../_resources/d6b055d5ff764a9aab6ea661da76b912.png)

    - Componenti:

        - Gate
        - Source
            - con la freccetta negli schemi
        - Drain
            - senza freccetta negli schemi
    - funziona come un BJT, ma con un meccanismo diverso

        - gli elettrodi sono isolati
            - se applichiamo una tensione al Gate creiamo un canale di conduzione che permette agli elettroni di passare da una parte all'altra del componente
                - "connette" Source e Drain utilizzando l'effetto di campo
                - questo vale per i MOSFET ad arricchimento
                    - nei jFET o nei MOSFET a svuotamento il comportamento e' duale
                        - Source e Drain sono normalmente connessi
                        - applicando una tensione al Gate si chiude il canale di conduzione ed il Transistor risulta aperto
    - è controllato in tensione e non in corrente

        - consuma meno energia (basta una tensione, nessuna corrente)
    - è più lento del BJT

        - si usa però sempre più spesso grazie ai progressi tecnologici

I circuiti con parte alta sul positivo (Active-High) sono più veloci rispetto a quelli con parte
alta sul negativo (Active-Low)

- sul filo viene caricata una capacità molto elevata (Ground)
    - più la resistenza è alta minore è la in-rush current
        - however la costante di tempo aumenta, e bisogna aspettare di più per poter leggere il
            valore di uscita in maniera corretta
            - in genrale si aspetta circa 3 o 4 costanti di tempo per avere una rilevazione corretta
            - i circuiti vanno pensati in base alle costanti di tempo per non incorrere in timing problems
                - ci dovrebbero venire in aiuto di hardwaristi
                - spesso pero' si misura invece a mano col prototipo con oscilloscopio, generatore di frequenze, osservando il comportamento a seguito di variazione della resistenza di pullup e delle capacità dei componenti
