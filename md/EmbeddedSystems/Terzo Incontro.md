Terzo Incontro

Le memorie cache non "partono" di default

- di base il sistema utilizza la memoria principale e le memorie cache devono essere attivate per  
    essere utilizzate
- per vari motivi
    - il sistema può partire ad una velocità più bassa
        - se la cache è troppo veloce per la CPU non è possibile utilizzarla
    - è una memoria limitata
        - consuma abbastanza energia ed ha uno spazio limitato
            - è una risorsa da utilizzare con parsimonia
    - alcune aree di memorie non devono essere gestite da una memoria cache
        - e.g. aree di I/O
- esistono dei registri che permettono di specificare quali aree di memoria utilizzare con la  
    cache
    - non ha senso, e.g., attivare la cache per zone di memoria che contengono codice di  
        inizializzazione
    - possono esistere delle porzioni di codice che "performance-critical" e risulta quindi  
        opportuno andare a "cachare" l'area di memoria principale che le contiene
        - tutto ciò a supporto della politica del cache controller
            - questa è la policy con cui vengono rimosse delle parole di memoria memorizzate nella  
                cache per fare spazio ad altre qualora diventi necessario
            - la cache solitamente si aggiorna a blocchi (e.g. di multipli di 4 word)
                - e.g. non viene caricata una sola istruzione, ma l'intero blocco minimo che la contiene
            - esistono registri per specificare la politica di caching da utilizzare
- le memorie cache sono solitamente divise in Data Cache e Program Cache
    - rispettivamente per dati e codice
    - bisogna stare più attenti con l'area dati
        - parte dell'address space del processore è allocata per l'accesso ai registri delle  
            periferiche (memory-mapped I/O)
            - questa è indicata nella memory map del processore
            - sarebbe errato gestire questi spazi di indirizzamento con una memoria cache
                - non bisogna **mai** attivare la cache per le aree di I/O

Se un processore accede ad un indirizzo di memoria a cui non corrisponde alcuna memoria fisica il  
valore letto dipenderà dalla configurazione del bus, dall'ultimo ciclo, etc.

- sarà totalmente casuale
- bisogna fare attenzione ed evitare che, ad esempio, il Program Counter cominci a riferirsi ad  
    indirizzi fuori dall'address space

Esistono indirizzamenti più complessi di quello lineare

- e.g. per indirection, per offset, per giustapposizione di registri, etc.

Esistono memorie di pipeline

- i.e. memorie parallele che permettono di eseguire frammenti di più operazioni nello stesso  
    istante
- queste possono portare a delle incertezze nell'utilizzo di un debugger
    - legato a:
        - ottimizzazioni del codice da parte del compilatore
        - utilizzo di memorie cache

Memorie

- schematicamente:

![Memory.png](../_resources/85214575a2294d148fafc6c1e45eca42.png)

- volatili
    - in genere contengono il programma
    - solitamente più veloci delle corrispettive non volatili
        - all'inizio del programma viene solitamente caricato il programma in RAM prima di essere  
            eseguito per una maggiore velocità
    - mantengono i dati finché sono alimentate
        - possono mantenerli per brevi periodi di tempo in mancanza di alimentazione
        - mai fidarsi dei dati se si sa che è mancata l'alimentazione
        - solitamente esiste un circuito che controlla i livelli di alimentazione e genera un segnale  
            di reset se il livello diventa troppo basso
            - in questo caso vengono reinizializzate le memorie ad esempio
            - due tipi:
                - black-out reset
                    - memorizzato in un registro specifico
                    - "notifica" il processore che sta partendo per via di una mancanza di alimentazione
                        - i.e. non in condizioni normali
                        - con un controllo si possono eseguire delle operazioni appropriate
                            - e.g. mandare un allarme
                - brown-out reset
                    - usano in genere una soglia più stringente del black reset
                        - e.g. cala la tensione di 1 volt rispetto al valore richiesto per operare
                    - il processore continua a funzionare per un breve intervallo di tempo
                        - si possono salvare i dati importanti
                        - si può notificare l'utilizzatore che il sistema sta per fermarsi
    - tra queste abbiamo le RAM
        - due tipi:
            - statiche
                - costituite da una batteria di Flip-Flop
            - dinamiche
                - più veloci
                    - mancano di alcuni componenti capacitive che rallentano l'utilizzo della memoria
- non volatili
    - in genere contengono i dati
    - ROM
        - Read Only Memory
            - possono essere solamente lette (dal processore)
                - in realtà possono essere scritte da un altro circuito prima di essere impiegate
        - esistono le PROM (Programmable ROM)
            - sotto certe condizioni possono essere anche scritte
                - memorie flash (e.g. chiavette USB)
                - sono scritte a blocchi e servono certe operazioni particolari per scriverle
- queste memorie tendono ad essere disponibili ad essere utilizzate sostanzialmente in qualsiasi istante
    - i.e. è sufficiente scrivere un indirizzo sul bus dati per poter leggere un valore o scriverne uno nuovo
        - in altre memorie è necessario anche un segnale di clock per ogni operazione di  
            lettura/scrittura
    - nei PC abbiamo memorie di tipo sincrono
    - memorie più lente scrivono i dati solo in corrispondenza di un fronte di salita o discesa del  
        clock
        - per ottenere maggiori prestazioni certe memorie utilizzano entrambi i fronti per la lettura/scrittura  
            dei dati
            - è il doppio più veloce di una memoria "lenta"
            - si parla in questo caso di Double Data Rate (DDR)

Un flip-flop può richiedere dai 4-6 MOSFET alle decine

- lo sforzo tecnologico è di occupare un piccolo spazio con tanti transistor
- poiché la densità non può essere troppo alta si è passati ad altri tipi di tecnologie per le  
    memorie dinamiche
    - queste sono basate su condensatori in combinazione con MOSFET
      ![MemoryCell.png](../_resources/33cda7ab8512486db9e8048734c3d18e.png)
      - richiedono un "rinfrescamento"
        - i.e. i dati vanno letti e riscritti periodicamente
        - i condensatori se lasciati a se stessi si vanno a scaricare
        - oggi la logica di refresh è contenuta nel chip
          - in passato era compito del processore
      - per accedere ai dati si utilizza un segnale di selezione di colonna (CAS) ed un segnale di
        selezione di riga (RAS) in due cicli
      - per via di questa tecnica di memorizzazione queste non sono affidabili
        - e.g. possono essere corrotte da radiazioni ionizzanti
        - non utilizzabili in certi ambiti applicativi

Le memorie flash utilizzano una tecnologia diversa

![FlashMemoryCell.png](../_resources/83c1fed75c464b8cab1f1fc8243b11c3.png)



- l'elemento base è il transistor a Gate flottante
    - l'elemento che mantiene la memoria è sul Gate
    - il tempo di mantenimento è lunghissimo
        - fino a 10 anni
    - il valore viene letto stimolando il transistor e andando a vedere se il transistor era carico  
        o meno
    - non possono essere scritte bit per bit come una RAM qualsiasi
        - devono essere scritte per blocchi
            - quando si accede ad un transistor si va ad accedere a tutta la batteria del transistor
- si utilizzato tensioni più altre rispetto alle memorie dinamiche
    - 10-12 volt
    - all'interno della memoria esistono circuiti che fanno da elevatori di tensione
- ad ogni operazione di scrittura si va a deteriorare i MOSFET
    - per via dei robusti impulsi di tensione necessari
    - ad un certo punto i componenti diventeranno non più in grado di mantenere i dati memorizzati
    - le letture sono harmless
        - le scritture vanno centellinate e non si deve esagerare
            - abbiamo circa 1000000 cicli di scrittura per le memorie più performanti
                - sembrano tanti, ma bisogna fare attenzione al codice
    - quando una cella si è degradata non si ha la certezza che non mantenga i dati
        - è solo probabile che non mantenga i dati
        - il tipo di degrado non è predicibile
        - esiste un limite minimo di scritture dato dal manifacturer
            - oltre quello le memorie andrebbero sostituite
- una memoria flash vergine contiene tutti 1
    - o è nuova o è appena stata cancellata
- quando un bit viene caricato si porta a zero
    - per ridurre l'usura le logiche all'interno del componente evitano di modificare i bit che sono  
        già ad 1
        - e.g. si scrivono solo i bit 0 necessari
- esistono aree nella memoria che si possono andare a guastare
    - esistono dei circuiti che fanno vedere all'esterno una nuova area al posto di quelle  
        danneggiate
        - si "autocorreggono"
        - sono componenti di fascia alta
            - in quelli di fascia bassa si fa attraverso il software
                - e.g. si può scrivere in celle via via crescenti per evitare di scrivere sempre le  
                    stesse celle

La CPU ha bisogno di comunicare con il mondo esterno

- cosa più semplice: comunicare su un filo
    - e.g. la CPU può avere un registro in cui scrivere le uscite, collegate ad un flip-flop  
        leggibile da fuori
    - questo è ok per pochi segnali relativamente vicini al processore
        - non scala bene all'aumento della distanza e della quantità dei segnali
- esistono interfacce un po' più avanzate
    - tra queste:
        - SPI (Serial Peripheral Interface)
		  ![SPI.png](../_resources/ef455ef3f0974e5b8fa31c75df020958.png)


            - si utilizza lo stesso filo per più bit
            - per indicare la presenza di un bit sul segnale abbiamo un segnale di clock
                - contando i fronti abbiamo il numero totale di bit trasmessi
                - il segnale di clock è dato da un cavo chiamato CLK
            - si hanno degli shift register
                - i bit vengono spinti verso "destra" uno alla volta ad ogni colpo di clock
            - non è Peer-to-Peer
                - esistono i ruoli di Master/Slave
                - abbiamo due cavi per la trasmissione/ricezione:
                    - MOSI (Master Out Slave In)
                    - MISO (Master In Slave Out)
                    - i segnali necessitano di una direzione in questo tipo di interfaccia
            - il numero di fili risulta limitato
                - costa meno
                - maggiore integrità dei segnali
            - la frequenza di clock è generata dal master
                - entro certi limiti dati dall'elettronica non è necessario che Master e Slave conoscano  
                    a priori la frequenza del clock
            - il clock oscilla solo quando ci sono dati in trasferimento
                - altrimenti il circuito rimane in uno stato di riposo
            - si può aggiungere un segnale di Slave Select (e.g. SS1) per decidere a quale Slave  
                permettere di parlare in presenza di più Slave 
                ![SPI_SlaveSelect.png](../_resources/20fc60e905fb4998b01919226505d25d.png)


                - il numero di fili rimane limitato
                - solitamente Active-Low
                - se necessario tutte le periferiche possono leggere il segnale dato sul MOSI
                    - tuttavia MISO può essere utilizzato solo quando si è interpellati (i.e. SS è attiva)
                - la connessione è di tipo tri-state
            - pensate per la comunicazione tra CPU e uno o più Slave sulla stessa scheda
                - brevissimi distante (e.g. 30 centimetri)
                - per distanze maggiori fa comodo eliminare il segnale clock
                    - si risparmia un filo
                    - all'aumento della distanza il segnale si degrada
        - I2C
            - un po' più complessa
        - UART
            - usata per la connessione tra due unità all'interno dello stesso telaio o addirittura  
                distanti metri
                - se vengono messi in mezzo oggetti come i Modem anche chilometri
            - Universal Asynchronous Receiver Transmitter
                - non esiste il concetto di Master e Slave
                    - P2P
                    - consente il trasferimento di dati in maniera full-duplex
                        - ricezione e trasmissione contemporanee
                        - realizzato grazie a due fili
                            - uno per la TX
                            - uno per la RX
            - si hanno anche due registri per la trasmissione e la ricezione
            - non c'è il clock
                - il clock è generato "localmente"
                    - questo marca gli istanti dei bit
                    - idealmente sarebbero in fase per sempre e partirebbero assieme
                        - in realtà la fase drifta col tempo e bisogna scandire il momento di inizio
                            - la sincronizzazione viene fatta marcando il bit di "start TX" ed il bit di "end  
                                TX"
                                ![UARTSynch.png](../_resources/6388b818ffc54c07b9e0d936cfe82646.png)


                                - normalmente la linea (di RX o TX) viene mantenuta ad un livello di riposo
                                    - alto in UART
                                - quando questo segnale cambia (si abbassa) UART immagina che sia cominciata la  
                                    trasmissione di una parola
                                    - viene quindi sincronizzato il clock locale per la ricezione di un numero di  
                                        bit configurato in precedenza (e.g. 8 bit)
            - bisogna mettersi d'accordo su alcuni aspetti fondamentali per far comunicare  
                correttamente due dispositivi mediante UART
                - la velocità di trasmissione
                - il numero di bit da trasmettere alla volta
                - il numero di stop bit
                    - il bit di start è sempre 1
                    - indica il tempo per cui la linea viene mantenuta nello stato di riposo prima di  
                        iniziare una nuova trasmissione
                        - usato dai circuti di UART per ricostruire il dato trasmesso in maniera asincrona
                            - per UART particolarmente lente servono in genere 2 bit di stop
                        - è un tempo minimo di attesa
                            - la linea può essere lasciata inattiva per intervalli temporali più lunghi
                - la parità
                    - usata come checksum per la detection di errori nella trasmissione
            - si hanno delle memorie di ricezione e trasmissione
                - sono FIFO (First In First Out)
                    - fanno uscire un dato appena ne entra uno fuori
                - fanno da buffer (tampone)
        - Per comunicazioni su lunghe distanze si utilizzano varie codifiche e non si possono usare i livelli  
            di tensione della scheda
            - su lunghe distanze il segnale si degrada
            - fenomeni capacitivi richiedono più energia nella trasmissione
            - all'uscita di un UART se si va a pilotare un componente a qualche metro di distanza si va ad  
                utilizzare un driver
                - è un dispositivo "stupido"
                    - e.g. trasforma l'ingresso da 3.3 volt in un'uscita da 10-12 volt con una corrente  
                        sufficiente a vincere gli ostacoli capacitivi
        - una codifica tipica richiede che la linea torni allo stato idle quando inutilizzata
            - Return to Zero encoding (RTZ)
                - alcuni svantaggi
                    - non si può trasferire il clock
                    - prevede uno stato di riposo
                        - un problema nelle reti di trasmissione
                    - per via delle capacità del cavo abbiamo un segnale in alternata che dipende  
                        dall'informazione trasmessa e dalla velocità di trasmissione
                        - ad un certo punto i receiver non riescono a discriminare quando un bit è a 1 o a 0
                - per sopperire a questi problemi sono state create altre codifiche
                    - e.g. Codifica di Manchester 
						![Manchester.png](../_resources/056332ea1daf48e89407488840dadf2d.png)


                        - codifica NRZ
                        - usata da Ethernet (802.3)
                        - il dato viene campionato sul fronte di discesa del clock
                            - in corrispondenza del campionamento se il dato è 1 viene trasmesso un fronte di  
                                salita
                            - in corrispondenza del campionamento se il dato è 0 viene trasmesso un fronte di  
                                discesa
                            - la prima versione lavorava in maniera opposta
                        - il segnale è alternato e centrato rispetto ad un valore medio
                            - si va dal positivo al negativo
                        - vantaggi
                            - il contenuto in continua è più alto
                                - si può trasmettere di più e per distanze più lunghe
                            - essendo il segnale in alternata può attraversare componenti capacitive o  
                                induttive
                                - i.e. si può isolare la componente dal computer ed evitare la presenza di  
                                    correnti parassite
                        - svantaggi:
                            - si occupa il doppio della banda rispetto al dato
                                - per tecnologie come Ethernet questo non è troppo problematico vista la banda  
                                    abbastanza larga
                        - il circuito di ricezione è molto semplice 

							![ManchesterRXCircuit.png](../_resources/3c998ac426664efd93176dcfb7cb71d3.png)


                            - il segnale in ingresso viene dato in pasto ad un Edge Detector
                                - abbiamo un 1 in uscita in corrispondenza di una transizione, 0 altrimenti
                                - e.g. filtro passa-alto o condensatore
                            - l'output dell Edge Detector viene dato ad un PLL
                                - oscilla con una frequenza comparabile a quella di clock
                                - senza divisore
                                    - restituisce esattamente il clock in ingresso
                            - l'output del PLL viene dato ad un circuito di Data Recostruction
                                - restituisce il dato esattamente come trasmesso
                        - due vantaggi:
                            - trasmissione sincrona nello stesso filo dove trasmettiamo il dato
                            - il contenuto di continua è molto basso
                                - la pause dell'UART non ci sono più

Oggi esistono componenti di processamento (e.g. microcontrollori) che hanno al loro interno sia la componente di elaborazione che quella di  
memoria (sia RAM che ROM)

- sono più adatte ad ambienti rumorosi (dal punto di vista elettromagnetico)
- consentono buone velocità

<!--vim:et:ts=2:sts=2:sw=2:
-->
